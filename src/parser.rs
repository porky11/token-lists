use crate::Token;
use token_parser::{Context, Parsable, Parser, Result, Unit};

impl From<Token> for Unit {
    fn from(value: Token) -> Self {
        use Token::*;
        match value {
            Symbol(name) => Self::Symbol(name),
            List(elements) => Self::Parser(Parser::new(elements)),
        }
    }
}

impl<C: Context> Parsable<C> for Token {
    fn parse_symbol(name: Box<str>, _context: &C) -> Result<Self> {
        Ok(Self::Symbol(name))
    }

    fn parse_list(parser: &mut Parser, context: &C) -> Result<Self> {
        Ok(Self::List(parser.parse_rest(context)?))
    }
}
