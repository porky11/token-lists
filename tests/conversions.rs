use token_lists::Token::{self, List, Symbol};

#[test]
fn from_str() {
    let token: Token = "symbol".into();
    assert_eq!(token, Symbol("symbol".into()))
}

#[test]
fn from_string() {
    let name: String = "symbol".into();
    let token: Token = name.into();
    assert_eq!(token, Symbol("symbol".into()))
}

#[test]
fn from_vec() {
    let token: Token = vec!["a", "b", "c"].into();
    assert_eq!(
        token,
        List(vec![
            Symbol("a".into()),
            Symbol("b".into()),
            Symbol("c".into())
        ])
    )
}
